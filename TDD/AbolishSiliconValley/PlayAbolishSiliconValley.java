class PlayAbolishSiliconValley {
    static int guesses = 0;

    public static void main(String[] args) {
        AbolishSiliconValley game = new AbolishSiliconValley();
        CliReader reader = new CliReader();
        String guess;

        System.out.println(welcomeMessage());

        while (!game.finished()) {
            guesses++;
            System.out.println(
                "\n" + game.guess(reader.input("Enter a guess: ")) + "\n"
            );
        }

        System.out.println(endingMessage());
    }

    static String welcomeMessage() {
        return "\n             Welcome to the Abolish Silicon Valley game!" +
"\n\nBillionaire tech oligarchs and their dotcom companies have invaded your" +
"\ncommunity and are sucking the very life out of it in their insatiable" +
" lust\nfor megaprofits.\n\n" +
"Your mission is to find and destroy them, replacing them with\n" +
"democratically run worker cooperatives, thus building wealth within\n" +
"your community and saving the lives of it's inhabitants. Good luck!\n";
    }

    static String endingMessage() {
        String s1 = "You did it! You've saved your community from" +
" the invading tech oligarchs.\n\nIt took you only ";
        String s2 = " guesses to do it.\n\n" +
"Your community is safe (for the time being) from their destructive power.\n" +
"\nGet busy developing community wealth through democratically run worker\n" +
"co-ops before they return!\n";

        return s1 + guesses + s2;
    }
}
