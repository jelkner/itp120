public class SimpleDotComGameDriver
{
    public static void main(String[] args)
    {
        GameHelper helper = new GameHelper();
        SimpleDotComGame game = new SimpleDotComGame();
        String guess;
        String whatHappened;


        while (game.stillPlaying())
        {
            guess = helper.getUserInput("Enter a number: ");
            whatHappened = game.takeAction(guess);
            System.out.println(whatHappened);
        }
        System.out.println(game.finished());
    }
}
