import java.io.*;

class TestFileReader {
    public static void main(String[] args) {
        FileGrabber f = new FileGrabber();
        String filePath = "/home/jelkner/Projects/itp120/Assorted/Hello.java";
        try {
            System.out.println(f.read(filePath));
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }
}
