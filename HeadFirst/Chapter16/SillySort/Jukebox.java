import java.util.*;
import java.io.*;

public class Jukebox {
    ArrayList<String> songList = new ArrayList<String>();

    class SillyCompare implements Comparator<String> {
        public int compare(String song1, String song2) {
            return song1.charAt(2) - song2.charAt(2);
        }
    }

    public static void main(String[] args) {
        new Jukebox().go();
    }

    public void go() {
        getSongs();
        System.out.println(songList);
        SillyCompare myCompare = new SillyCompare();
        Collections.sort(songList, myCompare);
        System.out.println(songList);
    }

    void getSongs() {
        try {
            File file = new File("../SongList.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = reader.readLine()) != null) {
                addSong(line);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    void addSong(String lineToParse) {
        String[] tokens = lineToParse.split("/");
        songList.add(tokens[0]);
    }
}
