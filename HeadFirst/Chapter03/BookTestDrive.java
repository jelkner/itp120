class BookTestDrive
{
    public static void main(String[] args)
    {
        Book[] myBooks = new Book[3];

        for (int i = 0; i < myBooks.length; i++)
        {
            myBooks[i] = new Book();
        }

        myBooks[0].title = "The Grapes of Java";
        myBooks[1].title = "The Java Gatsby";
        myBooks[2].title = "The Java Cookbook";

        myBooks[0].author = "Sue";
        myBooks[1].author = "Ian";
        myBooks[2].author = "Bob";

        for (Book book : myBooks)
        {
            System.out.println(book.title + " by " + book.author);
        }
    }
}
