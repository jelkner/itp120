public class Time
{
    private int hour;
    private int minute;
    private int second;
    
    public Time()
    {
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
    }
    
    public Time(int hour, int minute, int second)
    {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour()
    {
        return this.hour;
    }

    public void setHour(int h)
    {
        this.hour = h;
    }

    public int getMinute()
    {
        return this.minute;
    }

    public void setMinute(int m)
    {
        this.minute = m;
    }

    public int getSecond()
    {
        return this.second;
    }

    public void setSecond(int s)
    {
        this.second = s;
    }

    public String toString()
    {
        return String.format(
            "%d:%02d:%02d", this.hour, this.minute, this.second
        );
    }

    public Time addTime(Time t)
    {
        int sumSeconds = this.second + t.getSecond();
        int sumMinutes = this.minute + t.getMinute() + sumSeconds / 60;

        return new Time(
            this.hour + t.getHour() + sumMinutes / 60,
            sumMinutes % 60,
            sumSeconds % 60
        );
    }
}
