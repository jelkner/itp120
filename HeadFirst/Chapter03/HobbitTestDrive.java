class HobbitTestDrive
{
    public static void main(String[] args)
    {
        Hobbit[] hobbits = new Hobbit[3];

        for (int i = 0; i < hobbits.length; i++)
        {
            hobbits[i] = new Hobbit();
        }

        hobbits[0].name = "Bilbo";
        hobbits[1].name = "Frodo";
        hobbits[2].name = "Sam";

        for (Hobbit h : hobbits)
        {
            System.out.println(h.name + " is a good Hobbit name!");
        }
    }
}
