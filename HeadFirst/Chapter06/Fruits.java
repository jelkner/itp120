import java.util.*;

class Fruits
{
    public static void main(String[] args)
    {
        ArrayList<String> fruitList = new ArrayList<String>();
        String isOrNot;

        fruitList.add("blueberry");
        fruitList.add("banana");
        fruitList.add("cherry");
        fruitList.add("strawberry");

        System.out.println(
            "The fruit list has " + fruitList.size() + " fruits in it."
        );

        if (fruitList.contains("blueberry"))
        {
            isOrNot = " is ";
        }
        else
        {
            isOrNot = " is not ";
        }

        System.out.println("Blueberry" + isOrNot + "one of them.");
    }
}
