# Notes from Uncle Bob's Clean Code Episode 6: TDD Part 1

## Three Laws of TDD

1. Write no production code except to pass a failing test.
2. Write only enough of a test to demonstrate a failure.
3. Write only enough production code to pass the test.

## Three Characteristics of Rotting Code

1. Rigid
2. Fragile
3. Immobile

## Major Benefits of TDD

1. Reduction in debugging time (at least 50%).
2. An excellent low level design document.
3. Decoupled, "testable", code.

## Why You Need to Write Tests First

Writing tests *after* the code feels like make work. You already know your
code works since you tested it manually. Only by writing the tests first can
you develop the trust you need to have in them to effectively reduce fear of
refactoring.
