
/**
 * Abstract class Animal - All animals eat, and all animals communicate in
 * some way, which is being called emote here.
 *
 * @author Jeffrey Elkner
 * @version 0.1
 */
public abstract class Animal
{
    public abstract void eat(int nurishment);
    
    public abstract String emote();
}