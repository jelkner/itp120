/* 
Pieced together from:
https://stackoverflow.com/questions/4716503/reading-a-plain-text-file-in-java
*/
import java.io.*;

class FileGrabber {
    String everything;

    String read(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            everything = sb.toString();
        } finally {
            br.close();
        }
        return everything;
    }
}
