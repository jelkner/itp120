class SimpleDotComGame
{
    private int[] locations;
    private int hits = 0;
    private int guesses = 0;

    public SimpleDotComGame()
    {
        int start = (int)(Math.random() * 5);
        this.locations = new int[] {start, start + 1, start + 2};
    }

    public boolean stillPlaying()
    {
        return this.hits < 3;
    }

    public String takeAction(String userGuess)
    {
        int guess = Integer.parseInt(userGuess);

        // We're processing a guess, so count it.
        this.guesses++;

        for (int location : locations)
        {
            if (location == guess)
            { 
                this.hits++;
                if (this.hits > 2)
                    return "kill";
                return "hit";
            }
        }
        return "miss";
    }

    public String finished()
    {
        return "You took " + this.guesses + " guesses.";
    }
}
