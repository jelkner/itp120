
/**
 * Driver class to test cats.
 *
 * @author Jeffrey Elkner
 * @version 0.1
 */
public class CatTester
{
    public static void main(String[] args)
    {
        Cat felix = new Cat("Felix");
        
        System.out.println(felix.emote());
    }
}
