# Ready for Quidditch?

The game of [Quidditch](https://en.wikipedia.org/wiki/Quidditch) requires
seven players for a team. In order for the annual tournament at
[Hogwarts](https://en.wikipedia.org/wiki/Hogwarts), to take place, each of the
four houses (Gryffindor, Hufflepuff, Ravenclaw, and Slytherin) needs to field
their full complement of seven players, no more, no less.

Registration day for the tournament is chaotic. There is only a single Wizard
staffing the registration table, and students line up all day long to sign
up for the tournament.

At the end of the day, a single sign-up document is created with players
entered in the following format:

Harry Potter, Gryfindor
Luna Lovegood, Ravenclaw
Ignatia Wildsmith, Ravenclaw
Tom Riddle, Slytherin
Newt Scamander, Hufflepuff
...
Hermione Granger, Gryfindor
Draco Malfoy, Slytherin

The document has one player per line, with their name followed by their house
(team), separated by a comma.

Your task is to write a program that determines whether or not the tournament
list is complete. If there are exactly 7 players from each team, then it
should print out:

List complete, let's play quidditch!

If the list is not complete, it should print out a single line for each
team that does not meet the requirement. For example, it might print out:

Ravenclaw does not have enough players.
Hufflepuff has too many players.
