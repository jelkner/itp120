class Card
{
    public static final String[] suits =
    {
        "Clubs", "Diamonds", "Hearts", "Spades"
    };

    public static final String[] ranks =
    {
        "narf", "Ace", "Two", "Three", "Four", "Five", "Six", "Seven",
        "Eight", "Nine", "Ten", "Jack", "Queen", "King"
    };

    private int suit;
    private int rank;

    public Card(int suit, int rank)
    {
        this.suit = suit;
        this.rank = rank;
    }

    public String toString()
    {
        return Card.ranks[rank] + " of "  + Card.suits[suit];
    }
}
