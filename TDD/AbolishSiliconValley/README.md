# AbolishSiliconValley

I'm a community college instructor teaching Java to my students using
[Head First Java](https://www.allitebooks.in/head-first-java-2nd-edition/).

In chapters 5 and 6 of the book, students develop a
[Battleship](https://en.wikipedia.org/wiki/Battleship_(game)) like game the
book calls "Sink a Dot Com".

Inspired by my recent reading of Wendy Lui's book
[Abolish Silicon Valley](https://abolishsiliconvalley.com/) and motivated by
the distruction tech giant Amazon.com is already raining on what remains of
the working class community in Arlington, Virginia, where I live, I decided
to rename the project in honor of Wendy's book.
