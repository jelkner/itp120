class ElevenPointThree
{
    public static String bothStart(String a, String b)
    {
        int shortest = (a.length() <= b.length()) ? a.length() : b.length();
        int pos = 0;

        while (pos < shortest && a.charAt(pos) == b.charAt(pos)) 
            pos++;

        return a.substring(0, pos);
    }

    public static void main(String[] args)
    {
        System.out.println(bothStart(args[0], args[1]));
    }
}
