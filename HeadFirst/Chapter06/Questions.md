# Questions from Chapter 6: Using the Java Library

1. The very first page of the chapter talks about the **Java API**. What is
   the Java API? Why is it useful?

2. Describe the first option (a.k.a. "Option One" ;-) presented as a possible
   solution to our cliff hanger bug.

3. Why is option 1 rejected? What's wrong with it?

4. Describe option two. What does our book say is wrong with that?

5. Describe option three. What is the problem with this solution?

6. What "dreamy" new feature does the text present that would make option 3
   better?

7. Do you see any flaws in the solution presented in the chapter? What happens
   in the fixed version when the player guesses the same location more than
   once?
