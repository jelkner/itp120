class TestArrays
{
    public static void main(String[] args)
    {
        String[] islands = new String[4];
        int[] indices = {1, 3, 0, 2};

        islands[0] = "Bermuda";
        islands[1] = "Fiji";
        islands[2] = "Puerto Rico";
        islands[3] = "Hawaii";

        for (int i : indices)
        {
            System.out.println("island = " + islands[i]);
        }
    }
}
