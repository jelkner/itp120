class TestDotcom {
    public static void main(String[] args) {
        boolean tryHit;
        Dotcom amazon = new Dotcom("D2", 'H', "Amazon.com");
        Dotcom google = new Dotcom("B0", 'V', "Google");

        assert amazon.getName() == "Amazon.com":
            "Name test failed, got " + amazon.getName() == "Amazon.com";

        assert !amazon.isSunk():
            "isSunk test 1 failed, expected true, got " + !amazon.isSunk();

        tryHit = amazon.hit("D3");
        assert tryHit: "1st hit test failed, expected true, got " + tryHit;

        tryHit = amazon.hit("D3");
        assert !tryHit: "2nd hit test failed, expected true, got " + tryHit;

        tryHit = google.hit("C0");
        assert tryHit: "1st hit test failed, expected true, got " + tryHit;

        tryHit = google.hit("C0");
        assert !tryHit: "2nd hit test failed, expected true, got " + tryHit;

        amazon.hit("D2");
        assert !amazon.isSunk():
            "isSunk test 2 failed, expected true, got " + !amazon.isSunk();

        amazon.hit("D4");
        // should be sunk now
        assert amazon.isSunk():
            "isSunk test 3 failed, expected true, got " + amazon.isSunk();

        // Test collision detection
        Dotcom d1 = new Dotcom("D2", 'H', "Dot1");
        Dotcom d2 = new Dotcom("B0", 'V', "Dot2");
        Dotcom d3 = new Dotcom("B4", 'V', "Dot3");

        assert !d1.collidesWith(d2): "Failed non-colliding d1 d2 test";
        assert d1.collidesWith(d3): "Failed colliding d1 d3 test";
    }
}
