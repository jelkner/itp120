public class SimpleDotComTestDrive
{
    public static void main(String[] args)
    {
        SimpleDotCom dot = new SimpleDotCom();
        String userGuess;
        String result;

        int[] locations = {3, 4, 5};
        dot.setLocationCells(locations);

        assert dot.getHits() == 0 : "Expected 0, got " + dot.getHits();
        assert !dot.killed() : "Expected true, got false";
        assert dot.getGuesses() == 0 : "Expected 0, got " + dot.getGuesses();

        userGuess = "2";
        result = dot.checkYourself(userGuess);
        assert result == "miss" : "Expected miss, got " + result;
        assert dot.getGuesses() == 1 : "Expected 1, got " + dot.getGuesses();
        
        userGuess = "3";
        result = dot.checkYourself(userGuess);
        assert result == "hit" : "Expected hit, got " + result;
        assert dot.getHits() == 1 : "Expected 1, got " + dot.getHits();
    }
}
