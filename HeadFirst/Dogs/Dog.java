class Dog
{
    String name;

    public void bark()
    {
        System.out.println(name + " says Ruff!");
    }

    public static void main (String[] args)
    {
        Dog[] myDogs = new Dog[5];

        for (int i = 0; i < myDogs.length; i++) 
        {
            myDogs[i] = new Dog();
        }

        myDogs[0].name = "Bart";
        myDogs[1].name = "Fred";
        myDogs[3].name = "Marge";
        myDogs[4].name = "Foofie";

        for (Dog dog : myDogs)
        {
            dog.bark();
        }
    }
}
