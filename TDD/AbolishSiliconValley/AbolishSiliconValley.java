import java.util.ArrayList;
import java.util.Random;

class AbolishSiliconValley {
    ArrayList<Dotcom> dotcoms = new ArrayList<Dotcom>();
    Random rand = new Random();

    // Test constructor for placing Dotcoms in predetermined locations
    AbolishSiliconValley(ArrayList<Dotcom> dots) {
        dotcoms = dots;
    }

    // No argument constructor that places Dotcoms randomly 
    AbolishSiliconValley() {
        String pos;
        String[] names = {"Amazon.com", "Google", "Facebook"}; 
        String rows = "ABCDE";

        while (dotcoms.size() < 3) {
            int r = rand.nextInt(5);
            char dir = (rand.nextInt(2) == 0) ? 'V': 'H';
            pos = rows.substring(r,r+1) + Integer.toString(rand.nextInt(5));
            tryToAddDotcom(new Dotcom(pos, dir, names[dotcoms.size()]));
        }
    }

    boolean tryToAddDotcom(Dotcom newDot) {
        for (Dotcom dot: dotcoms) {
            if (dot.collidesWith(newDot)) {
                return false;
            }
        }
        dotcoms.add(newDot);
        return true;
    }

    int numDotcoms() {
        return dotcoms.size();
    }

    boolean finished() {
        return dotcoms.size() == 0;
    }

    String guess(String cell) {
        for (int i = 0; i < dotcoms.size(); i++) {
            Dotcom dot = dotcoms.get(i);
            String name = dot.getName();
            if (dot.hit(cell)) {
                if (dot.isSunk()) {
                    dotcoms.remove(i);
                    return "Congratulations, you destroyed " +  name + "!"; 
                } 
                return "hit";
            }
        }
        return "miss";
    }
}
