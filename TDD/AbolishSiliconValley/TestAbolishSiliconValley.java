import java.util.ArrayList;

class TestAbolishSiliconValley {
    public static void main(String[] args) {
        String guess;

        AbolishSiliconValley game = new AbolishSiliconValley();

        assert game.numDotcoms() == 3:
            "numDotcoms test 1 failed, expected 3 got " + game.numDotcoms();

        // Create testGame with know locations for Dotcoms 
        ArrayList<Dotcom> dotcoms = new ArrayList<Dotcom>();
        dotcoms.add(new Dotcom("D2", 'H', "Amazon.com"));
        dotcoms.add(new Dotcom("B0", 'V', "Google"));
        dotcoms.add(new Dotcom("G3", 'H', "Facebook"));
        AbolishSiliconValley testGame = new AbolishSiliconValley(dotcoms);

        assert !testGame.finished(): "finished test 1 failed.";
        guess = testGame.guess("A3");
        assert guess == "miss":
            "Test guess A3 failed. Expected miss, got, " + guess;

        guess = testGame.guess("B2");
        assert guess == "miss":
            "Test guess B2 failed. Expected miss, got, " + guess;

        guess = testGame.guess("D2");
        assert guess == "hit":
            "Test guess D2 failed. Expected hit, got, " + guess;

        guess = testGame.guess("D3");
        assert guess == "hit":
            "Test guess D3 failed. Expected hit, got, " + guess;

        guess = testGame.guess("D4");
        assert guess.startsWith("Congratulations"):
            "Test guess D4 failed. Expected starts with 'Congratulations'," +
            " got, " + guess;

        testGame.guess("B0");
        testGame.guess("C0");
        testGame.guess("D0");

        assert testGame.numDotcoms() == 1: "Failed test removing 2nd Dotcom";
        assert !testGame.finished(): "Failed 1 left finished test";

        testGame.guess("G3");
        testGame.guess("G4");
        testGame.guess("G5");

        assert testGame.numDotcoms() == 0: "Failed test removing 3rd Dotcom";
        assert testGame.finished(): "Failed game over finished test";
    }
}
