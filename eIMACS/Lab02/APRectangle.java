 /**
 *
 * @author Jeff Elkner
 * @version 1.0 2019-10-24
 */
import java.awt.Graphics;

public class APRectangle
{
    private APPoint myTopLeft;
    private double myWidth;
    private double myHeight;
    
    public APRectangle( APPoint topLeft, double width, double height )
    {
        myTopLeft = topLeft;
        myWidth = width;
        myHeight = height;
    }
    
    public APPoint getTopLeft()
    {
        return myTopLeft;
    }
    
    public double getWidth()
    {
        return myWidth;
    }
    
    public double getHeight()
    {
        return myHeight;
    }
    
    public void setTopLeft( APPoint topLeft )
    {
        myTopLeft = topLeft;
    }
    
    public void setWidth( double width )
    {
        myWidth = width;
    }
    
        public void setHeight( double height )
    {
        myHeight = height;
    }
    
    public APPoint getTopRight()
    {
        return new APPoint( myTopLeft.getX() + myWidth, myTopLeft.getY() );
    }
    
    public APPoint getBottomLeft()
    {
        return new APPoint( myTopLeft.getX(), myTopLeft.getY() + myHeight );
    }
    
    public APPoint getBottomRight()
    {
        return new APPoint( myTopLeft.getX() + myWidth, myTopLeft.getY() + myHeight );
    }
    
    public void shrink( double percent )
    {
        myWidth *= percent / 100.0;
        myHeight *= percent / 100.0;
    }
    
    public void draw(Graphics g)
    {
        APPoint topLeft = myTopLeft;
        APPoint topRight = getTopRight();
        APPoint bottomLeft = getBottomLeft();
        APPoint bottomRight = getBottomRight();

        g.drawLine((int)topLeft.getX(), (int)topLeft.getY(),
                   (int)topRight.getX(), (int)topRight.getY());
        g.drawLine((int)topRight.getX(), (int)topRight.getY(),
                   (int)bottomRight.getX(), (int)bottomRight.getY());
        g.drawLine((int)topLeft.getX(), (int)topLeft.getY(),
                   (int)bottomLeft.getX(), (int)bottomLeft.getY());
        g.drawLine((int)bottomLeft.getX(), (int)bottomLeft.getY(),
                   (int)bottomRight.getX(), (int)bottomRight.getY());
    }
}
