public class CheckItem
{
    private double myPrice, mySalesTax;
    private int myQuantity = 1;

    public static double roundMoney(double amount)
    {
        return (int)(100 * amount + 0.5) / 100.00;
    }

    public CheckItem(double price, double salesTax)
    {
        myPrice = price;
        mySalesTax = salesTax;
    }

    public int getQuantity()
    {
        return myQuantity; 
    }

    public void setQuantity(int qty)
    {
        myQuantity = qty; 
    } 

    public double lineItemTotal()
    {
        return roundMoney(myPrice * myQuantity * (1 + mySalesTax)); 
    }
}
