
/**
 * Defines a cat that can eat and meow.
 *
 * @author Jeffrey Elkner
 * @version 0.1
 */
public class Cat extends Animal
{
    // an integer between 0 and 100 indicating how hungry the cat is.
    private int fullness;
    private String name;

    /**
     * Set fullness to 0.
     */
    public Cat(String aName)
    {
        // initialise instance variables
        fullness = 0;
        name = aName;
    }

    public String emote()
    {
        return "Meow, my name is " + name + "!";
    }
    
    public void eat(int food)
    {
        if (fullness - food < 100)
        {
            fullness += food;
        }
    }
    
    public String hungerStatus()
    {
        if (fullness < 20)
        {
            return "I am *very* hungry. Please feed me!";
        }
        else if (fullness < 80)
        {
            return "I could eat a little bit, but I don't need to.";
        }
        return "I'm full, no more food or I'll burst!";
    }
}
