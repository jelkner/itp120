class CheckItemTester
{
    public static void main(String[] args)
    {
        CheckItem item = new CheckItem(12.95, 6.0);
        item.setQuantity(2);

        System.out.println("$" + item.lineItemTotal());
    }
}
