import java.util.ArrayList;
import java.util.Arrays;

class Dotcom {
    private String name;
    private ArrayList<String> cells = new ArrayList<String>();
    private boolean[] hits = new boolean[3];

    Dotcom(String pos, char orientation, String setName) {
        name = setName;
        cells.add(pos);
        if (orientation == 'V') {
            String col = pos.substring(1,2);
            int rowVal = (int)pos.charAt(0);
            cells.add(String.valueOf((char)(rowVal+1) + col)); 
            cells.add(String.valueOf((char)(rowVal+2) + col)); 
        } else {  // orientation == 'H'
            String row = pos.substring(0,1);
            int colVal = (int)pos.charAt(1);
            cells.add(row + String.valueOf((char)(colVal+1))); 
            cells.add(row + String.valueOf((char)(colVal+2))); 
        }
        // Print cells for visual inspection
        // System.out.println(Arrays.toString(cells.toArray()));
    }

    ArrayList<String> getCells() {
        return cells;
    }

    String getName() {
        return name;
    }

    boolean hit(String cell) {
        int index = cells.indexOf(cell);

        // if cell is outside Dotcom or it's been hit before return false
        if (index == -1 || hits[index]) {
            return false;
        }
        // 1st hit, so set it and return true
        hits[index] = true;
        return true;
    }

    boolean isSunk() {
        for (boolean hit: hits) {
            if (!hit) {
                return false;
            }
        }
        return true;
    }

    boolean collidesWith(Dotcom dot) {
        for (String cell: cells) {
            if (dot.getCells().indexOf(cell) != -1) {
                return true;
            }
        }
        return false;
    }
}
